package de.richtercloud.pmd.identical.catchh.branches;

public class Exception1 extends Exception {
    private static final long serialVersionUID = 1L;
    
    public Exception1(String message) {
        super(message);
    }

    public Exception1(String message,
            Throwable cause) {
        super(message,
                cause);
    }

    public Exception1(Throwable cause) {
        super(cause);
    }
}
