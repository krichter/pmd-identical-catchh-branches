package de.richtercloud.pmd.identical.catchh.branches;

public class Exception0 extends Exception {
    private static final long serialVersionUID = 1L;

    public Exception0(String message) {
        super(message);
    }

    public Exception0(String message,
            Throwable cause) {
        super(message,
                cause);
    }

    public Exception0(Throwable cause) {
        super(cause);
    }
}
